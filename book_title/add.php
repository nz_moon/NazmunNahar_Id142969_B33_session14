<!DOCTYPE html>
<html lang="en">
<head>
    <title>Add Book</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../assest/css/bootstrap.min.css">
    <link rel="stylesheet" href="add_style.css">
    <script src="../assest/js/bootstrap.min.js"></script>
    <script src="../assest/jquery/jquery.min.js"></script>

</head>
<div class="container">
    <h2>Add Book</h2>
    <form>
        <div class="form-group">
            <label for="book_title">Book Title:</label>
            <input type="book_title" class="form-control" id="book_title" placeholder="Enter Book Title">
        </div>
        <div class="form-group">
            <label for="author_name">Autor Name:</label>
            <input type="author_name" class="form-control" id="author_name" placeholder="Enter Autor Name">
        </div>

        <div class="" align="center">
        <button type="submit" class="btn btn-default">Add</button>
        <button type="submit" class="btn btn-default">Save & Add</button>
        <button type="reset" class="btn btn-default">Reset</button>
        <button type="submit" class="btn btn-default">Back To List</button>
        </div>
    </form>
</div>

</body>
</html>

