<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Book</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../assest/css/bootstrap.min.css">
    <link rel="stylesheet" href="add_style.css">
    <script src="../assest/js/bootstrap.min.js"></script>
    <script src="../assest/jquery/jquery.min.js"></script>

</head>
<div class="container">
    <h2>Edit Book</h2>
    <form>
        <div class="form-group">
            <label for="edit_book_title">Edit Book Title:</label>
            <input type="edit_book_title" class="form-control" id="edit_book_title" placeholder="Enter Book Title">
        </div>
        <div class="form-group">
            <label for="edit_author_name">Edit Autor Name:</label>
            <input type="edit_author_name" class="form-control" id="author_name" placeholder="Enter Autor Name">
        </div>

        <div class="" align="center">
            <button type="submit" class="btn btn-default">Edit/Update</button>
            <button type="reset" class="btn btn-default">Reset</button>
            <button type="submit" class="btn btn-default">Delete</button>
            <button type="submit" class="btn btn-default">Back To List</button>
        </div>
    </form>
</div>

</body>
</html>

