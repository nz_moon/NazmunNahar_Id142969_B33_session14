<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../assest/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
    <script src="../assest/js/bootstrap.min.js"></script>
    <script src="../assest/jquery/jquery.min.js"></script>

</head>
<body>

<div class="container">
    <h2><center>Book title</center></h2>
    <p></p>
    <table class="table">
        <thead>
        <tr class="table-bordered">
            <th >Serial</th>
            <th>Id</th>
            <th>Book Title</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <tr class="table-bordered">
            <td>1</td>
            <td>5</td>
            <td>PHP-BOOK Title#1</td>
            <td>
                <button type="button" class="btn btn-default">view</button>
                <button type="button" class="btn btn-default">Edit</button>
                <button type="button" class="btn btn-default">Delete</button>
                <button type="button" class="btn btn-default">Trash</button>
            </td>
        </tr>
        <tr class="table-bordered">
            <td>2</td>
            <td>6</td>
            <td>PHP-BOOK Title#2</td>
            <td>
                <button type="button" class="btn btn-default">view</button>
                <button type="button" class="btn btn-default">Edit</button>
                <button type="button" class="btn btn-default">Delete</button>
                <button type="button" class="btn btn-default">Trash</button>
            </td>
        </tr>
        <tr class="table-bordered">
            <td>3</td>
            <td>7</td>
            <td>PHP-BOOK Title#3</td>
            <td>
                <button type="button" class="btn btn-default">view</button>
                <button type="button" class="btn btn-default">Edit</button>
                <button type="button" class="btn btn-default">Delete</button>
                <button type="button" class="btn btn-default">Trash</button>
            </td>
        </tr>
        <tr class="table-bordered">
            <td colspan="4">PAGE:<1,2,3,4,5,6,7></td>

        </tr>
        <tr class="table-bordered ">
            <td colspan="4" align="center">
                <button type="button" class="btn btn-default">Add New Books Title</button>
                <button type="button" class="btn btn-default">View Trash Items</button>
                <button type="button" class="btn btn-default">download as PDF</button>
                <button type="button" class="btn btn-default">download as EXCEL File</button>
            </td>


        </tr>

        </tbody>
    </table>
</div>

</body>
</html>

