<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Birthdate</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../assest/css/bootstrap.min.css">
    <link rel="stylesheet" href="add_style.css">
    <script src="../assest/js/bootstrap.min.js"></script>
    <script src="../assest/jquery/jquery.min.js"></script>

</head>
<div class="container">
    <h2>Edit Birthdate</h2>
    <form>
        <div class="form-group">
            <label for="edit_name">Edit Name:</label>
            <input type="edit_name" class="form-control" id="edit_name" placeholder="Enter Your Name">
        </div>
        <div class="form-group">
            <label for="edit_birthdate">Edit Birthdate:</label>
            <input type="date" class="form-control" id="date" placeholder="Enter Birthdate">
        </div>

        <div class="" align="center">
            <button type="submit" class="btn btn-default">Edit/Update</button>
            <button type="reset" class="btn btn-default">Reset</button>
            <button type="submit" class="btn btn-default">Delete</button>
            <button type="submit" class="btn btn-default">Back To List</button>
        </div>
    </form>
</div>

</body>
</html>

