<!DOCTYPE html>
<html lang="en">
<head>
    <title>Add Birthday</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../assest/css/bootstrap.min.css">
    <link rel="stylesheet" href="add_style.css">
    <script src="../assest/js/bootstrap.min.js"></script>
    <script src="../assest/jquery/jquery.min.js"></script>

</head>
<div class="container">
    <h2>Add Birthday</h2>
    <form>
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="name" class="form-control" id="name" placeholder="Enter Your Name">
        </div>
        <div class="form-group">
            <label for="birthdate">Birthdate:</label>
            <input type="date" class="form-control" id="date" placeholder="Enter Your Birthdate">
        </div>

        <div class="" align="center">
            <button type="submit" class="btn btn-default">Add</button>
            <button type="submit" class="btn btn-default">Save & Add</button>
            <button type="reset" class="btn btn-default">Reset</button>
            <button type="submit" class="btn btn-default">Back To List</button>
        </div>
    </form>
</div>

</body>
</html>

