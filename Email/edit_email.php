<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Email</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../assest/css/bootstrap.min.css">
    <link rel="stylesheet" href="add_style.css">
    <script src="../assest/js/bootstrap.min.js"></script>
    <script src="../assest/jquery/jquery.min.js"></script>

</head>
<body>

<div class="container">
    <h2>Edit Email</h2>
    <form action="">
        <div class="form-group">
            <label for="email">Edit Email:</label>
            <input type="email" class="form-control" id="email" placeholder="Enter Your email">
        </div>
        <div class="form-group">
            <label for="url">Enter URL:</label>
            <input type="url" class="form-control" id="url" placeholder="Enter URL">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>
